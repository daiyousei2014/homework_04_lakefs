import pandas as pd

from sklearn.preprocessing import LabelEncoder
from sklearn.linear_model import LogisticRegression
from sklearn.naive_bayes import GaussianNB
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis

from joblib import dump

models = {
    "lr": LogisticRegression(
        random_state=snakemake.params.random_seed,
        n_jobs=snakemake.threads,
        max_iter=1000,
    ),
    "nb": GaussianNB(),
    "lda": LinearDiscriminantAnalysis(),
}

encoder = LabelEncoder()

df = pd.read_csv(snakemake.input.train)
X_train = df[df.columns[:-1]]
y_train = encoder.fit_transform(df[df.columns[-1]])

model = models.get(
    snakemake.params.model,
    LogisticRegression(random_state=snakemake.params.random_seed),
)
model.fit(X_train, y_train)

dump(encoder, snakemake.output.encoder)
dump(model, snakemake.output.model)
