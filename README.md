# MLops homework: LakeFS

This is a homework repository for the [MLOps & production for data science research 3.0](https://ods.ai/tracks/mlops3-course-spring-2024).

This is based on Snakemake howework, but with less models and two dataset versions instead ('production', which is 50% of the original, and 'staging', which is the same as the original, this simulating gathering more data). See docs/dag.md for the graph and description.

## Running:

`conda env create -f dev.yaml`

`conda run -n dev snakemake --cores all`
