import os
import lakefs
from lakefs.client import Client

client = Client(
    username=os.environ.get("LAKECTL_CREDENTIALS_ACCESS_KEY_ID"),
    password=os.environ.get("LAKECTL_CREDENTIALS_SECRET_ACCESS_KEY"),
    host="animeco.in:8000",
)

repo = lakefs.Repository(repository_id="homework", client=client).create(
    storage_namespace="local://homework_lakefs", exist_ok=True
)


def upload(repo, branch, filename, object_name, message, base="main"):
    try:
        repo.branch(branch).delete()
    except lakefs.exceptions.NotFoundException:
        pass

    br = repo.branch(branch).create("main")

    with (
        open(filename, mode="rb") as reader,
        br.object(object_name).writer(mode="wb") as writer,
    ):
        writer.write(reader.read())
    br.commit(message)


upload(repo, "production", "upload/data_smol.csv", "data.csv", "Add data v.1")
upload(
    repo,
    "staging",
    "upload/data.csv",
    "data.csv",
    "Add data v.2",
    base="production",
)
