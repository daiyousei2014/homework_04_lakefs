import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

from sklearn.metrics import (
    accuracy_score,
    roc_auc_score,
    roc_curve,
    precision_recall_curve,
    confusion_matrix,
    average_precision_score,
)
from sklearn.preprocessing import label_binarize

from joblib import load


def calculate_metrics(target_test, predictions, probabilities):
    acc = accuracy_score(target_test, predictions)
    cmatrix = confusion_matrix(target_test, predictions)

    (
        ap,
        roc_auc,
        fpr,
        tpr,
        precision,
        recall,
        thresholds,
        f_scores,
        best_f,
        best_thresh,
    ) = ([0 for i in class_indices] for j in range(10))

    target_vector = label_binarize(target_test, classes=class_indices)
    for i in class_indices:
        ap[i] = average_precision_score(
            target_vector[:, i], probabilities[:, i]
        )
        fpr[i], tpr[i], _ = roc_curve(target_vector[:, i], probabilities[:, i])
        roc_auc[i] = roc_auc_score(target_vector[:, i], probabilities[:, i])

        precision[i], recall[i], thresholds[i] = precision_recall_curve(
            target_vector[:, i], probabilities[:, i]
        )
        # You may adjust the beta here:
        beta = 1
        f_scores[i] = (
            (1 + beta**2)
            * recall[i]
            * precision[i]
            / (recall[i] + beta**2 * precision[i] + np.finfo(float).eps)
        )
        best_thresh[i] = thresholds[i][np.argmax(f_scores[i])]
        best_f[i] = np.max(f_scores[i])

    optimal_threshold_preds = np.argmax((probabilities > best_thresh), axis=1)
    best_cmatrix = confusion_matrix(target_test, optimal_threshold_preds)

    return (
        best_f,
        roc_auc,
        acc,
        ap,
        best_thresh,
        beta,
        fpr,
        tpr,
        recall,
        precision,
        cmatrix,
        best_cmatrix,
    )


def visualize(target_test, probabilities):
    predictions = probabilities.argmax(axis=1)
    fig, axes = plt.subplots(1, 2, figsize=(10, 4))
    axes[0].plot([0, 1], linestyle="--")
    axes[1].plot([0.5, 0.5], linestyle="--")

    (
        best_f,
        roc_auc,
        acc,
        ap,
        best_thresh,
        beta,
        fpr,
        tpr,
        recall,
        precision,
        cmatrix,
        best_cmatrix,
    ) = calculate_metrics(target_test, predictions, probabilities)
    for i in class_indices:
        axes[0].plot(
            fpr[i],
            tpr[i],
            label=f' {encoder.classes_[i]} - ROC_AUC: {roc_auc[i]:.2f}, AP (PR_AUC): {ap[i]:.2f}, best {r"$F_{"+str(beta)+r"}$"}: {best_f[i]:.2f} @ {best_thresh[i]:.2f} threshold',
        )
        axes[1].plot(recall[i], precision[i])
    axes[0].legend(bbox_to_anchor=(2.0, -0.8), loc="lower right")

    axes[0].set(
        xlabel="FPR",
        ylabel="TPR",
        title="ROC curves",
        xlim=(0, 1),
        ylim=(0, 1),
    )
    axes[1].set(
        xlabel="Recall",
        ylabel="Precision",
        title="PR curves",
        xlim=(0, 1),
        ylim=(0, 1),
    )
    plt.savefig(snakemake.output.metrics, bbox_inches="tight")

    fig, axes = plt.subplots(1, 2, figsize=(10, 4), layout="constrained")
    labs = encoder.classes_

    for cmatrix, ax, title in zip(
        [cmatrix, best_cmatrix],
        axes.flat,
        ["Confusion Matrix", "Confusion Matrix (optimal threshold)"],
    ):
        sns.heatmap(cmatrix, ax=ax, annot=True, cmap="Blues", fmt="d").set(
            title=title,
            xlabel="Prediction",
            ylabel="Reality",
            xticks=np.arange(0.5, len(labs) + 0.5, 1.0),
            yticks=np.arange(0.5, len(labs) + 0.5, 1.0),
        )
        ax.set_xticklabels(labs, rotation=90)
        ax.set_yticklabels(labs, rotation=0)

    plt.savefig(snakemake.output.cmatrix)

    return best_thresh


encoder = load(snakemake.input.encoder)
model = load(snakemake.input.model)

class_indices = range(len(encoder.classes_))

df = pd.read_csv(snakemake.input.test)
X_test = df[df.columns[:-1]]
y_test = encoder.transform(df[df.columns[-1]])

proba = model.predict_proba(X_test)

visualize(y_test, proba)
