# Snakemake pipeline

- download the dataset;
- preprocess the dataset: drop missing data, split the test set, apply feature scaling (either StandardScaler() or PowerTransformer());
- train models: linear regression, naive Bayes, linear discriminant analysis; save models and target encoder;
- evaluate models against the test set.

## DAG
![DAG](report/dag.svg)
